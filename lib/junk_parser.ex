defmodule JunkParser do
  def parse_files(files) do
    Enum.filter_map files,
      &(Regex.match?(~r/.+\.junk/i, &1)),
      &parse/1
  end

  def parse(file) do
    File.read!(file)
    |> (&(String.split(&1, "\n"))).()
    |> (fn (lines) -> (Enum.map(lines, &String.strip/1)) end).()
    |> (&(parse(&1, [], %Parser{name: file}))).()
  end

  defp parse([line = "Name " <> name|rest], parsed, parser) do
    p = %Modulle{name: name,
                 line: line,
                 line_number: parser.line_number}

    parse(rest,
          [p | parsed],
          %Parser{parser|line_number: parser.line_number + 1})
  end

  defp parse([line = "Function " <> name|rest], parsed, parser) do
    p = %Funxion{function: name,
                 line: line,
                 line_number: parser.line_number}

    {lines, parser} = parse_function_body(rest, p, parser)
    f = %Funxion{p|lines: lines}

    parse(rest, [f | parsed], parser)
  end

  defp parse([_line | rest], parsed, parser) do
    parse(rest, parsed, %Parser{parser|line_number: parser.line_number + 1})
  end

  defp parse([], parsed, parser) do
    {:ok, f} = File.open("parser-results.txt", [:append])

    IO.write(f, "\#\#\# FILE: #{parser.name} \#\#\#")

    Enum.each(Enum.reverse(parsed), fn(p) ->
      case p do
        %Modulle{} -> IO.write(f, "\n\n\#\# #{p.name}")
        %Funxion{} ->
          IO.write(f, "\n\n\t\t #{p.line_number}: #{p.function}")
          Enum.each(Enum.reverse(p.lines), fn(l) ->
            IO.write(f, "\n\t\t\t #{l}")
          end)
      end
    end)

    IO.write(f, "\n\n#\#\n\n")

    File.close f
  end

  defp parse_function_body(["End"|_], funxion, parser) do
    {funxion.lines, parser}
  end

  defp parse_function_body([line | rest], funxion, parser) do
    f = %Funxion{funxion|lines: [line|funxion.lines]}

    parse_function_body(rest, f, %Parser{parser|line_number: parser.line_number + 1})
  end
end

