# clear the results file
results_file = "parser-results.txt"
{:ok, f} = File.open(results_file, [:write])
IO.write(f, "")
File.close f

path = "./test-files"
{:ok, files} = File.ls(path)
JunkParser.parse_files(Enum.map(files, &(Path.expand(&1, path))))
IO.puts :ok

